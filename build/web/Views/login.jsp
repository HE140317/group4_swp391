<%-- 
    Document   : login
    Created on : Jun 7, 2022, 12:53:12 AM
    Author     : Hà Mai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<form action="login" method="post">
    <h1>${message}</h1>
    <label>Email</label>
    <input type="email" name="email" placeholder="Email" required="">
    <br>
    <label>Password</label>
    <input type="password" name="password" placeholder="Password" required="">
    <button type="submit" class="btn-style1">Sign in</button>
</form>